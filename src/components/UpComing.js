import { useEffect, useState } from "react";
import axios from "axios";
import { useNavigate } from "react-router";

const UpComing = () => {
  const [listUpComing, setListUpComing] = useState([]);
  useEffect(() => {
    axios
      .get(
        `https://api.themoviedb.org/3/movie/upcoming?api_key=${process.env.REACT_APP_API_KEY}&language=en-US&page=1`
      )
      .then((res) => {
        setListUpComing((prevsValue) => {
          return [...prevsValue, ...res.data.results];
        });
      });
    return function cleanup() {
      setListUpComing([]);
    };
  }, [setListUpComing]);
  const navigate = useNavigate();
  function goToDetails(movie_id) {
    navigate({
      pathname: `/movie/${movie_id}`,
    });
  }
  return (
    <>
      <h2>Up Coming</h2>
      <div className="parent">
        {listUpComing.length > 0
          ? listUpComing.map((item, index) => {
              return (
                <div
                  key={index}
                  className="child"
                  onClick={() => goToDetails(item.id)}
                >
                  <img
                    src={`https://image.tmdb.org/t/p/w500${item.poster_path}`}
                    alt="Poster"
                  />
                  <div className="content">
                    <p className="title">{item.title}</p>
                    <p>{new Date(item.release_date).getFullYear()}</p>
                  </div>
                </div>
              );
            })
          : "Empty"}
      </div>
      <hr />
    </>
  );
};

export default UpComing;
