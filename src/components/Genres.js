import axios from "axios";
import { useEffect, useState } from "react";
import { useNavigate } from "react-router";
import Genre from "./Genre";

const Genres = () => {
  const [listGenre, setListGenre] = useState([]);
  useEffect(() => {
    axios
      .get(
        `https://api.themoviedb.org/3/genre/movie/list?api_key=${process.env.REACT_APP_API_KEY}&language=en-US`
      )
      .then((res) => {
        setListGenre((prevsValue) => {
          return [...prevsValue, ...res.data.genres];
        });
      });
    return function cleanup() {
      setListGenre([]);
    };
  }, [setListGenre]);
  const navigate = useNavigate();
  function goToDetails(movie_id) {
    navigate({
      pathname: `/movie/${movie_id}`,
    });
  }
  return (
    <>
      {listGenre.length > 0
        ? listGenre.map((genre, index) => {
            return <Genre id={genre.id} name={genre.name} key={index} goToDetails={goToDetails}/>;
          })
        : "Empty List genre"}
    </>
  );
};

export default Genres;
