import axios from "axios";
import { useEffect, useState } from "react";
import { useNavigate } from "react-router";

const NowPlaying = () => {
  const [listNowPlaying, setListNowPlaying] = useState([]);
  useEffect(() => {
    axios
      .get(
        `https://api.themoviedb.org/3/movie/now_playing?api_key=${process.env.REACT_APP_API_KEY}&language=en-US&page=1`
      )
      .then((res) => {
        setListNowPlaying((prevsValue) => {
          return [...prevsValue, ...res.data.results];
        });
      });
    return function cleanup() {
      setListNowPlaying([]);
    };
  }, [setListNowPlaying]);
  const navigate = useNavigate();
  function goToDetails(movie_id) {
    navigate({
      pathname: `/movie/${movie_id}`,
    });
  }
  return (
    <>
      <h2>Now Playing</h2>
      <div className="parent">
        {listNowPlaying.length > 0
          ? listNowPlaying.map((item, index) => {
              return (
                <div key={index} className="child" onClick={() => goToDetails(item.id)}>
                  <img
                    src={`https://image.tmdb.org/t/p/w500${item.poster_path}`}
                    alt="Poster"
                  />
                  <div className="content">
                    <p className="title">{item.title}</p>
                    <p>{new Date(item.release_date).getFullYear()}</p>
                  </div>
                </div>
              );
            })
          : "Empty"}
      </div>
      <hr />
    </>
  );
};

export default NowPlaying;
