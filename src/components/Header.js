import {
  Navbar,
  Button,
  Nav,
  Container,
  FormControl,
  Form,
} from "react-bootstrap";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import { authActions } from "../store/auth";

import "../components/style.css";

const Header = () => {
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const username = useSelector((state) => state.auth.username);
  function logout() {
    dispatch(authActions.logout());
  }
  function search(e) {
    e.preventDefault();
    navigate({
      pathname: "/search",
      search: `?keyword=${e.target.search.value}`,
    });
  }
  return (
    <Navbar bg="dark" variant="dark" expand="lg">
      <Container>
        <Navbar.Brand>
          <Nav.Link href="/">My Movie</Nav.Link>
        </Navbar.Brand>
      </Container>

      <Container className="justify-content-center">
        <Form className="d-flex" onSubmit={search}>
          <FormControl
            type="search"
            placeholder="Search"
            className="me-2"
            aria-label="Search"
            style={{ width: "100%" }}
            name="search"
          />
          <Button variant="outline-success" type="submit">
            Search
          </Button>
        </Form>
      </Container>

      <Container>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="justify-content-end" style={{ width: "100%" }}>
            <h4 style={{ color: "white", marginRight:"10px" }}>Halo, {username}</h4>
            <Button variant="success" onClick={logout}>
              Sign Out
            </Button>
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
};

export default Header;
