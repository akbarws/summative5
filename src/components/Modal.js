import { Modal } from "react-bootstrap";

function MyModal(props) {
  return (
    <Modal
      {...props}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header >
        <Modal.Title id="contained-modal-title-vcenter-hcenter" style={{margin:"auto"}}>
          NEW MOVIE
        </Modal.Title>
      </Modal.Header>
      <Modal.Body style={{margin:"auto"}}>
        <h4 style={{textAlign:"center", color:"green"}}>{props.data.original_title}</h4>
        <img
          src={`https://image.tmdb.org/t/p/w500${props.data.poster_path}`}
          alt="No Pict"
        />
      </Modal.Body>
    </Modal>
  );
}

export default MyModal;
