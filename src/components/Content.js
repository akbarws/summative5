import Trending from "./Trending";
import TopRated from "./TopRated";
import Popular from "./Popular";
import NowPlaying from "./NowPlaying";
import UpComing from "./UpComing";
import Genres from "./Genres";

import "./style.css";

const Content = () => {
  return <div className="content-container">
  <Trending />
  <TopRated />
  <Popular />
  <NowPlaying />
  <UpComing />
  <Genres />
  </div>
};

export default Content;
