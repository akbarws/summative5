import { useState } from "react";
import { Col, Form, Row, Button } from "react-bootstrap";
import { useDispatch } from "react-redux";
import { Link } from "react-router-dom";
import { authActions } from "../store/auth";

const Auth = () => {
  const dispatch = useDispatch();
  const [form, setForm] = useState({
    username: "",
    password: "",
  });
  function formHandler(e) {
    setForm((prev) => {
      return { ...prev, [e.target.name]: e.target.value };
    });
  }
  const login = (e) => {
    e.preventDefault();
    dispatch(authActions.login(form));
  };
  return (
    <div>
      <Row>
        <Col>
          <h1 className="mt-5 p-3 text-center rounded">Login</h1>
        </Col>
      </Row>
      <Row>
        <Col md={{ span: 4, offset: 4 }}>
          <Form onSubmit={login}>
            <Form.Group className="loginForm p-4" as={Row}>
              <Form.Label>Username</Form.Label>
              <Form.Control
                type="text"
                name="username"
                placeholder="Enter email"
                onChange={(e) => formHandler(e)}
              />
            </Form.Group>

            <Form.Group className="loginForm p-4" as={Row}>
              <Form.Label>Password</Form.Label>
              <Form.Control
                type="password"
                name="password"
                placeholder="Password"
                onChange={(e) => formHandler(e)}
              />
            </Form.Group>

            <Form.Group className="loginForm p-4" as={Row}>
              <Button variant="success" type="submit">
                Log in
              </Button>
            </Form.Group>
          </Form>
        </Col>
      </Row>
      <Row>
        <Col md={{ span: 4, offset: 4 }}>
          <Link to="/">
            <Form.Group className="registerForm p-4" as={Row}>
              <Button variant="primary">Back</Button>
            </Form.Group>
          </Link>
        </Col>
      </Row>
    </div>
  );
};

export default Auth;
