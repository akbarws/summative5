import { useEffect, useState } from "react";
import axios from "axios";
import { useNavigate } from "react-router-dom";

const Trending = () => {
  const [listTrending, setListTrending] = useState([]);
  useEffect(() => {
    axios
      .get(
        `https://api.themoviedb.org/3/trending/movie/week?api_key=${process.env.REACT_APP_API_KEY}&language=en-US&page=1`
      )
      .then((res) => {
        setListTrending((prevsValue) => {
          return [...prevsValue, ...res.data.results];
        });
      });
      return function cleanup() {
        setListTrending([]);
      };
  }, [setListTrending]);
  const navigate = useNavigate();
  function goToDetails(movie_id) {
    navigate({
      pathname: `/movie/${movie_id}`,
    });
  }
  return (
    <>
      <h2>Trending This Week</h2>
      <div className="parent">
        {listTrending.length > 0
          ? listTrending.map((item, index) => {
              return (
                <div
                  key={index}
                  className="child"
                  onClick={() => goToDetails(item.id)}
                >
                  <img
                    src={`https://image.tmdb.org/t/p/w500${item.poster_path}`}
                    alt="Poster"
                  />
                  <div className="content">
                    <p className="title">{item.title}</p>
                    <p>{new Date(item.release_date).getFullYear()}</p>
                  </div>
                </div>
              );
            })
          : "Empty"}
      </div>
      <hr />
    </>
  );
};

export default Trending;
