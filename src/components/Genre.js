import { useEffect, useState } from "react";
import axios from "axios";

const Genre = (props) => {
  const [listMovieByGenre, setListMovieByGenre] = useState([]);
  useEffect(() => {
    axios
      .get(
        `https://api.themoviedb.org/3/discover/movie?api_key=${process.env.REACT_APP_API_KEY}&language=en-US&sort_by=popularity.desc&with_genres=${props.id}`
      )
      .then((res) => {
        setListMovieByGenre((prevsValue) => {
          return [...prevsValue, ...res.data.results];
        });
        return function cleanup() {
          setListMovieByGenre([]);
        };
      });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [setListMovieByGenre]);
  return (
    <>
      <h2>{props.name}</h2>
      <div className="parent">
        {listMovieByGenre.length > 0
          ? listMovieByGenre.map((item, index) => {
              return (
                <div key={index} className="child" onClick={() => props.goToDetails(item.id)}>
                  <img
                    src={`https://image.tmdb.org/t/p/w500${item.poster_path}`}
                    alt="Poster"
                  />
                  <div className="content">
                    <p className="title">{item.title}</p>
                    <p>{new Date(item.release_date).getFullYear()}</p>
                  </div>
                </div>
              );
            })
          : "Empty"}
      </div>
      <hr />
    </>
  );
};

export default Genre;
