import { useEffect, useState } from "react";
import axios from "axios";
import { useNavigate } from "react-router-dom";

const Similar = (props) => {
  const [listSimilar, setListSimilar] = useState([]);
  useEffect(() => {
    axios
      .get(
        `https://api.themoviedb.org/3/movie/${
          props.movie_id
        }/similar?api_key=${localStorage.getItem(
          "API_KEY"
        )}&language=en-US&page=1`
      )
      .then((res) => {
        setListSimilar((prevsValue) => {
          return [...prevsValue, ...res.data.results];
        });
      });
    return function cleanup() {
      setListSimilar([]);
    };
  }, [setListSimilar, props.movie_id]);
  const navigate = useNavigate();
  function goToDetails(movie_id) {
    navigate({
      pathname: `/movie/${movie_id}`,
    });
  }
  return (
    <div style={{ marginTop: "15px" }}>
      <h2>Similar</h2>
      <div className="parent">
        {listSimilar.length > 0
          ? listSimilar.map((item, index) => {
              return (
                <div
                  key={index}
                  className="child"
                  onClick={() => goToDetails(item.id)}
                >
                  <img
                    src={`https://image.tmdb.org/t/p/w500${item.poster_path}`}
                    alt="Poster"
                  />
                  <div className="content">
                    <p className="title">{item.title}</p>
                    <p>{new Date(item.release_date).getFullYear()}</p>
                  </div>
                </div>
              );
            })
          : "Empty"}
      </div>
      <hr />
    </div>
  );
};

export default Similar;
