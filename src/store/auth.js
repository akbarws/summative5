import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  isAuth: false,
  username: "",
};

const authSlice = createSlice({
  name: "auth",
  initialState: initialState,
  reducers: {
    login(state, data) {
      if (
        data.payload.username === "akbar" &&
        data.payload.password === "akbar"
      ) {
        return {
          ...state,
          isAuth: true,
          username: data.payload.username,
        };
      }
    },
    logout(state) {
      state.isAuth = false;
    },
  },
});

export const authActions = authSlice.actions;

export default authSlice.reducer;
