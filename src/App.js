import "./App.css";
import { Routes, Route } from "react-router-dom";
import Home from "./screens/Home";
import Movie from "./screens/Movie";
import Search from "./screens/Search";
import NotFound from "./screens/NotFound";
import Auth from "./components/Auth";
import { useSelector } from "react-redux";
import { Fragment } from "react";

function App() {
  const isAuth = useSelector((state) => state.auth.isAuth);
  return (
    <Fragment>
      {isAuth ? (
        <Routes>
          <Route path="/" element={<Home />}></Route>
          <Route path="/movie/:movie_id" element={<Movie />}></Route>
          <Route path="/search" element={<Search />}></Route>
          <Route path="*" element={<NotFound />}></Route>
        </Routes>
      ) : (
        <Auth />
      )}
    </Fragment>
  );
}

export default App;
