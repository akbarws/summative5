import axios from "axios";
import { Fragment, useEffect, useState } from "react";
import Content from "../components/Content";
import Header from "../components/Header";
import MyModal from "../components/Modal";

const Home = () => {
  const [modalShow, setModalShow] = useState(false);
  const [newMovie, setNewMovie] = useState({});
  useEffect(() => {
    axios
      .get(
        `https://api.themoviedb.org/3/movie/latest?api_key=${process.env.REACT_APP_API_KEY}&language=en-US`
      )
      .then((res) => {
        if (res.data.poster_path != null) {
          setNewMovie((prevsValue) => {
            return { ...prevsValue, ...res.data };
          });
          setModalShow(true);
        }
      });
    return () => {
      setModalShow(false);
    };
  }, [setModalShow]);
  return (
    <Fragment>
      <Header />
      <Content />
      <MyModal
        show={modalShow}
        onHide={() => setModalShow(false)}
        data={newMovie}
      />
    </Fragment>
  );
};

export default Home;
