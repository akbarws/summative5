import axios from "axios";
import { useEffect, useState } from "react";
import { Link, useParams } from "react-router-dom";
import { Button, Col, Container, Modal, Row } from "react-bootstrap";
import Recommendation from "../components/Recommendation"

import "./movie.css";
import Similar from "../components/Similar";

const Movie = () => {
  const params = useParams();
  const [movie_id, setMovie_id] = useState(params.movie_id);
  const [movieDetail, setMovieDetail] = useState({});
  const [genres, setGenres] = useState([]);
  const [videos, setVideos] = useState([]);
  
  useEffect(() => {
    setMovie_id(params.movie_id);
    axios
      .get(
        `https://api.themoviedb.org/3/movie/${movie_id}?api_key=${localStorage.getItem(
          "API_KEY"
        )}&language=en-US`
      )
      .then((res) => {
        setMovieDetail((prevsValue) => {
          return { ...prevsValue, ...res.data };
        });
        setGenres((prevsValue) => {
          return [...prevsValue, ...res.data.genres];
        });
      });
    axios
      .get(
        `https://api.themoviedb.org/3/movie/${movie_id}/videos?api_key=${localStorage.getItem(
          "API_KEY"
        )}&language=en-US`
      )
      .then((res) => {
        setVideos((prevsValue) => {
          return [...prevsValue, ...res.data.results];
        });
      });
      return function cleanup() {
        setMovieDetail([]);
        setVideos([]);
        setGenres([]);
        setMovie_id(null);
      };
  }, [setMovieDetail, setVideos, params.movie_id, movie_id]);
  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);
  return (
    <Container style={{ padding: "5px" }}>
      <Button className="btn-back" variant="success">
        <Link className="btn-back-link" to="/">
          Back
        </Link>
      </Button>
      <Row>
        <Col sm={5}>
          <img
            className="detail-image"
            src={`https://image.tmdb.org/t/p/w500${movieDetail.poster_path}`}
            alt="Poster"
          />
        </Col>
        <Col sm={7} >
          <Row>
            <h1>
              {movieDetail.title}&nbsp;
              <span style={{ color: "grey", fontSize: "20pt" }}>
                ({new Date(movieDetail.release_date).getFullYear()})
              </span>
            </h1>
            <p>
              {genres.length > 0
                ? genres
                    .map((genre) => {
                      return [genre.name];
                    })
                    .join(", ")
                : "belum"}
            </p>
          </Row>
          <Row>
            <h3>Overview</h3>
            <p>{movieDetail.overview}</p>
          </Row>
          <Row>
            <h4>Status</h4>
            <p>{movieDetail.status}</p>
          </Row>
          <Row>
            <h4>Videos</h4>
            <div className="video-parent">
              {videos.length === 0 ? "-" : ""}
            {videos.map((video) => {
              return (
                <div className="video-child">
                  <Button
                    variant="success"
                    style={{ margin: "5px" }}
                    onClick={handleShow}
                  >
                    {video.name}
                  </Button>
                  <Modal show={show} onHide={handleClose}>
                    <iframe
                      width="560"
                      height="315"
                      src={`https://www.youtube.com/embed/${video.key}`}
                      title="YouTube video player"
                      frameborder="0"
                      allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                      allowfullscreen
                    ></iframe>
                  </Modal>
                </div>
              );
            })}
            </div>
          </Row>
        </Col>
      </Row>
      <Recommendation movie_id={movie_id} />
      <Similar movie_id={movie_id} />
    </Container>
  );
};

export default Movie;
