import { Button } from "react-bootstrap";
import React, { useEffect, useState } from "react";
import { useLocation } from "react-router";

import "./style.css";
import axios from "axios";
import { Link } from "react-router-dom";

function useQuery() {
  const { search } = useLocation();

  return React.useMemo(() => new URLSearchParams(search), [search]);
}

const Search = () => {
  let query = useQuery();
  const [listSearch, setListSearch] = useState([]);
  useEffect(() => {
    axios
      .get(
        `https://api.themoviedb.org/3/search/movie?api_key=${localStorage.getItem(
          "API_KEY"
        )}&language=en-US&page=1&include_adult=false&query=${query.get(
          "keyword"
        )}`
      )
      .then((res) => {
        console.log(res.data.results);
        setListSearch((prevsValue) => {
          return [...prevsValue, ...res.data.results];
        });
      });
  }, [setListSearch, query]);
  return (
    <div className="searchbody">
      <Button variant="success"><Link className="black" to="/">Back</Link></Button>
      <h1>Search Movie : {query.get("keyword")}</h1>
      <div className="searchParent">
        {listSearch.map((item) => {
          return <div className="searchChild"><img src={`https://image.tmdb.org/t/p/w500${item.poster_path}`} alt="" /></div>;
        })}
      </div>
    </div>
  );
};

export default Search;
